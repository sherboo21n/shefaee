import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shefaee/Model/signUpModel.dart';


class AuthenticationPI{

  Future<UserModel> createUser(
      String name , String email , String phone , String phoneKey , String address , String password , String confirmPassword) async {
    final String apiUrl = "https://shafaey.jadara.work//api/user/register";
    Map<String , dynamic> body = {
      "name" : name,
      "email" : email,
      "phone" : phone,
      "phone_key" : phoneKey,
      "password" : password,
      "password_confirmation" : confirmPassword,
      "address" : address,
      "platform" : "ios",
      "device_id" : "124859hyrdckjhgdrdhutfhj",
      "token" : "kuyretgduyvbfifhfufbfydfjh",
      "lat" : 37.43296265331129,
      "lng" : -122.08832357078792
    };
    final response = await http.post(apiUrl,body: body );
    if(response.statusCode == 201){
      var jsonData = jsonDecode(response.body);
      var message = jsonData["message"];
      print(message);
    } else{
      print(response.body);
    }
  }
}


