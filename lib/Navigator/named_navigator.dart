class Routes {
  static const SIGNUP_ROUTER = "SIGNUP_ROUTER";
  static const GOOGLE_MAP = "GOOGLE_MAP";

}

abstract class NamedNavigator {
  Future push(String routeName,
      {dynamic arguments, bool replace = false, bool clean = false});

  void pop({dynamic result});
}
