import 'package:flutter/material.dart';
import 'package:shefaee/Themes/colors.dart';
import 'Navigator/named_navigator.dart';
import 'Navigator/named_navigator_impl.dart';

void main() =>runApp(MyApp());

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> navKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      key: navKey,
      title: 'شفائي',
      initialRoute: Routes.SIGNUP_ROUTER,
      onGenerateRoute: NamedNavigatorImpl.onGenerateRoute,
      navigatorKey: NamedNavigatorImpl.navigatorState,
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        primaryColorBrightness: Brightness.light,
        brightness: Brightness.light,
        primaryColor: Color(lightThemeColors['primary']),
        accentColor: Color(lightThemeColors['secondary']),
        backgroundColor: Color(0xFFF4FAFD),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

