import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shefaee/Ui/Auth/SignUp/Components/sign_up_item.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  final _title = [
    "تسجيل الدخول",
    "حساب جديد"
  ];
  int _selectedIndex = 0;
  var _view = [];


  @override
  void initState() {
    _view = [
      Text(""),
     SignUpItem()
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                "assets/images/background.jpg"
              ),
              fit: BoxFit.fill
            )
          ),
          child: ListView(
            shrinkWrap: true,
            children: [
              SizedBox(
                height: 70,
              ),
              SizedBox(
                height: 60,
              ),
              Container(
                height: 150,
                margin: EdgeInsets.only(
                  right:  30
                ),
                child: Center(
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: _title.length,
                      itemBuilder: (context , index) => InkWell(
                        onTap: (){
                          setState(() {
                            _selectedIndex = index;
                          });
                        },
                        child: Row(
                          children: [
                            Text(
                              _title[index],
                              style: TextStyle(
                                  fontSize: 24,
                                  color: _selectedIndex == index ? Theme.of(context).accentColor : Colors.grey.shade500
                              ),
                            ),
                            SizedBox(
                              width: 40,
                            )
                          ],
                        ),
                      )
                  ),
                ),
              ),
              _view[_selectedIndex]
            ],
          ),
        ),
      ),
    );
  }
}
