import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shefaee/Model/signUpModel.dart';
import 'package:shefaee/Navigator/named_navigator.dart';
import 'package:shefaee/Navigator/named_navigator_impl.dart';
import 'package:shefaee/Response/signUpAuth.dart';
import 'package:shefaee/Ui/Commen/custom_textField.dart';
import 'package:http/http.dart' as http;


class SignUpItem extends StatefulWidget {
  @override
  _SignUpItemState createState() => _SignUpItemState();
}

class _SignUpItemState extends State<SignUpItem> {

  GlobalKey<FormState> form_key = GlobalKey();

  TextEditingController _name = TextEditingController();
  TextEditingController _emailAddress = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _phoneKey = TextEditingController();
  TextEditingController _address = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

  AuthenticationPI _authenticationPI = AuthenticationPI();

  void _data() async {
    if (!form_key.currentState.validate()) {
      return;
    }
    await _authenticationPI.createUser(
        _name.text,
        _emailAddress.text,
        _phone.text,
        _phoneKey.text,
        _password.text,
        _confirmPassword.text,
        _confirmPassword.text
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: form_key,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CustomTextField(
            hintText: "إسم المستخدم",
            inputType: TextInputType.text,
            controller: _name,
            validate: (String value) {
              if (value == null || value.length == 0) {
                return "برجاء إدخال الاسم" ;
              }
            },
          ),
          CustomTextField(
            hintText: "البريد الإلكتروني",
            inputType: TextInputType.emailAddress,
            controller: _emailAddress,
            validate: (String value) {
              if (value == null ||
                  !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                      .hasMatch(value)) {
                return "برجاء إدخال البريد الإلكتروني";
              }
            },
          ),
          CustomTextField(
            hintText: "رقم الجوال",
            hintColor:Colors.purpleAccent.shade700,
            inputType: TextInputType.phone,
            controller: _phone,
            prefixIcon:  CountryCodePicker(
              initialSelection: "IT",
              onChanged: (controller){
                setState(() {
                  controller == _phoneKey;
                });
              },
            ),
            validate: (String value) {
              if (value == null || value.length == 0) {
                return "برجاء إدخال رقم الجوال" ;
              }
            },
          ),
          CustomTextField(
            hintText: "الدولة",
            inputType: TextInputType.text,
            controller: _address,
            suffixIcon:  Icon(
                Icons.location_on,
                color: Colors.grey.shade600,
              ),
            validate: (String value) {
              if (value == null || value.length == 0) {
                return "برجاء إدخال الدولة" ;
              }
            },
            onTap: (){
              var mNamedNavigator = NamedNavigatorImpl();
              mNamedNavigator.push(
                  Routes.GOOGLE_MAP, replace: true, clean: true
              );
            }
          ),
          CustomTextField(
            hintText: "كلمة المرور",
            obscure: true,
            inputType: TextInputType.visiblePassword,
            controller: _password,
            validate: (String value) {
              if (value == null || value.length < 5) {
                return "برجاء إدخال كلمة المرور" ;
              }
            },
          ),
          CustomTextField(
            hintText: "تأكيد كلمة المرور",
            obscure: true,
            inputType: TextInputType.visiblePassword,
            controller: _confirmPassword,
            validate: (String value) {
              if (value == null || value.length < 5) {
                return "كلمة المرور غير مطابقة" ;
              }
            },
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: _data,
            child: Container(
                height: 60,
                margin: EdgeInsets.symmetric(
                    horizontal: 90
                ),
                decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.circular(30)
                ),
                child: Center(
                  child: Text(
                    "تسجيل",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                )
            ),
          ),
          SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }

}







