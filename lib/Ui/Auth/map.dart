import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class googleMap extends StatefulWidget {
  @override
  _googleMapState createState() => _googleMapState();
}

class _googleMapState extends State<googleMap> {

  Set<Marker> _marker = {};
  void _oMapCreated(GoogleMapController){
    setState(() {
      _marker.add(
        Marker(
            markerId: MarkerId("id-1") ,
            position: LatLng(37.43296265331129, -122.08832357078792),

        )
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Stack(
          children: [
            GoogleMap(
              onMapCreated: _oMapCreated,
              markers: _marker,
              initialCameraPosition: CameraPosition(
                target: LatLng(37.43296265331129, -122.08832357078792),
                zoom: 15,
              ),
            ),
            Positioned(
              top: 40,
              left: 20,
              right: 20,
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextField(
                  decoration: InputDecoration(
                    enabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 10
                    ),
                    hintText: "البحث",
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 18
                    ),
                    suffixIcon:  Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 20,
              left: 100,
                right: 100,
              child: RaisedButton(
                onPressed: (){},
                child: Text(
                  "حفظ",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold
                  ),
                ),
                color: Color(0xFF9E9E9E),
              ),
            )
          ],
        )
      ),
    );
  }
}

