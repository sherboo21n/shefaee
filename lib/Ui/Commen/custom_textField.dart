import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {

  final String hintText;
  final bool obscure;
  final Function validate;
  final Function onSubmitted;
  final Function onChanged;
  final Function onTap;
  final TextEditingController controller;
  final TextInputType inputType;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final hintColor;

  CustomTextField({
    Key key,
  this.hintText,
    this.onTap,
    this.obscure,
  this.controller,
  this.inputType,
    this.hintColor,
  this.onChanged,
  this.onSubmitted,
  this.validate,
    this.prefixIcon,
    this.suffixIcon
}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85,
      margin: EdgeInsets.symmetric(
          horizontal: 40,
      ),
      child: Center(
        child: TextFormField(
          keyboardType: inputType,
          obscureText: obscure ?? false,
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(
                fontSize: 20,
                color: hintColor ?? Colors.grey.shade700
            ),
            contentPadding: EdgeInsets.only(
                top: 10.0,
                bottom: 10.0,
                left: 10,
                right: 10
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(
                color: Theme.of(context).primaryColor,
              ),
            ),
            border: OutlineInputBorder(
              borderRadius:  BorderRadius.circular(30),
              borderSide:  BorderSide(),
            ),
            errorStyle: TextStyle(
                fontSize: 18,
            ),
            errorMaxLines: 4,
            suffixIcon: suffixIcon ?? null,
            prefixIcon: prefixIcon ?? null
          ),
          // ignore: missing_return
          validator: validate,
          onChanged: onChanged,
          onFieldSubmitted: onSubmitted,
          controller: controller,
          onTap: onTap ?? (){},
        ),
      ),
    );
  }
}
