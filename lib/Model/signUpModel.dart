import 'dart:convert';

class UserModel{
  final String name;
  final String email;
  final String address;
  final String phone;
  final String phoneKey;
  final String password;
  final String passwordConfirm;

  UserModel({
    this.phoneKey,
    this.address,
    this.password,
    this.phone,
    this.passwordConfirm,
    this.email,
    this.name
});

}
